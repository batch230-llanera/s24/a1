const getCube = Math.pow(2,3)

console.log(`the cube of 2 is ${getCube}`)

const address = ["258 Washington Ave NW", "California", 90011];
const [street, city, zip] = address

console.log(`I live at ${street}, ${city} ${zip}`);

const animal = {
	name: "Lolong",
	type: "saltwater crocodile",
	weight: 1075,
	height: {
		ft: 20,
		in: 3
	}
}

const {name, type, weight, height} = animal
console.log(`${name} was a ${type}. He weighed ${weight} kgs with a measurement of ${height.ft} ft ${height.in} in.`)


const numbers = [1, 2, 3, 4, 5]

numbers.forEach((numbers) => {
	console.log(`${numbers}`);
})

let i = 0;
    let reduceNumber = numbers.reduce((acc,cur) => {
    	++i;
    	return acc + cur;
    });
    
    console.log(reduceNumber);

class Dog {
	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}

const myDog = new Dog('"Frankie"', 5, '"Miniature Dachshund"');
console.log(myDog);
